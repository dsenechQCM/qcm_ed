vpath %.hpp .. ../src 
vpath %.cpp .. ../src

INCLUDE = -I ../src

OBJS= \
binary_state.o \
console.o \
continued_fraction_set.o \
continued_fraction.o \
destruction_operator.o \
ED_basis.o \
qcm_ED.o \
global_parameter.o \
Hund_operator.o \
Heisenberg_operator.o \
Lanczos.o \
matrix.o \
model_instance_base.o \
model_instance.o \
model.o \
parser.o \
sector.o \
symmetry_group.o \
vector_num.o \
w_integrate.o \
qcm_ED_Py.o



