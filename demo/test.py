#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import qcm_ED as ed
import matplotlib.pyplot as plt

ed.new_model("2x2-C2v", 4, 0, np.array([[2,1,4,3],[3,4,1,2]]))

ed.new_operator("2x2-C2v", "U", "interaction", 
	np.array([
		(0, 4, 1.0),
		(1, 5, 1.0),
		(2, 6, 1.0),
		(3, 7, 1.0)
		], dtype=[('r','i8'),('c','i8'),('v','f8')]))

ed.new_operator("2x2-C2v", "D", "anomalous", 
	np.array([
		(0, 5, -1.0),
		(1, 4, -1.0),
		(0, 6,  1.0),
		(2, 4,  1.0),
		(1, 7,  1.0),
		(3, 5,  1.0),
		(2, 7, -1.0),
		(3, 6, -1.0),
		(5, 0,  1.0),
		(4, 1,  1.0),
		(6, 0, -1.0),
		(4, 2, -1.0),
		(7, 1, -1.0),
		(5, 3, -1.0),
		(7, 2,  1.0),
		(6, 3,  1.0)
		], dtype=[('r','i8'),('c','i8'),('v','f8')]))

ed.new_operator("2x2-C2v", "t", "one-body", 
	np.array([
		(0, 1, -1.0),
		(1, 0, -1.0),
		(2, 0, -1.0),
		(0, 2, -1.0),
		(1, 3, -1.0),
		(3, 1, -1.0),
		(2, 3, -1.0),
		(3, 2, -1.0),
		(4, 5, -1.0),
		(5, 4, -1.0),
		(4, 6, -1.0),
		(6, 4, -1.0),
		(5, 7, -1.0),
		(7, 5, -1.0),
		(6, 7, -1.0),
		(7, 6, -1.0)
		], dtype=[('r','i8'),('c','i8'),('v','f8')]))


ed.new_operator("2x2-C2v", "mu", "one-body", 
	np.array([
		(0, 0, -1.0),
		(1, 1, -1.0),
		(2, 2, -1.0),
		(3, 3, -1.0),
		(4, 4, -1.0),
		(5, 5, -1.0),
		(6, 6, -1.0),
		(7, 7, -1.0)
		], dtype=[('r','i8'),('c','i8'),('v','f8')]))

ed.new_model_instance("2x2-C2v", 0, {'U' : 0.0, 't' : 1.0, 'mu' : 0.0, 'D' : 0.5} , "R0:S0")

S = ed.ground_state_solve(0)
print('energie du fondamental : ', S[0])
print("secteur(s) de l'état fondamental : ", S[1])
ave = ed.ground_state_averages(0)
for x in ave:
	print('moyenne de ', x[0], ' : ', x[1])

ed.Green_function_solve(0)

eta = 0.05j
g = ed.Green_function(0,1+eta)
print("matrice de Green:", g)

g = ed.self_energy(0,1+eta)
print("self-énergie:", g)

g = ed.hopping_matrix(0);
print("matrice de saut = "  , g)

d = ed.Green_function_dimension(0)
print("dimension de la matrice de Green: ", d)

# plotting the spectral function
a = 4
w = np.arange(-a,a,0.01)
nw = len(w)
A = np.zeros((nw, d))
for i in range(nw) :
	g = ed.Green_function(0, w[i]+eta)
	A[i,:] = -2*np.diagonal(g).imag

np.savetxt('g.out', np.hstack((np.reshape(w,(len(w),1)), A)), fmt='%1.5f', delimiter='\t')

offset = 10
for i in range(d):
	plt.plot(w, A[:,i] + i*offset, color = 'b')
plt.xlabel('$\omega$')
plt.xlim(-a,a)
plt.title('cluster spectral function, 2x2-C2v')
plt.show()




#ed.print_models()
