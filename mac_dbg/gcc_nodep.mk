#Your favorite compiler goes here.
COMPILER	= g++-8

#linker; Should be the compiler in most cases.
#A notable exception in making a MPI app using clang to compile, then the linker should be mpicxx
LINKER	= $(COMPILER)

PYPREFIX = $(shell python3-config --prefix)

#dynamic link library will vary quite a bit from one platform to the other
LINK	= -framework Accelerate \
	-L$(PYPREFIX)/lib $(shell python3-config --libs)

#include path, should be left empty for most platform
LOCAL_INCLUDE 	= -I/usr/local/include/  \
	$(shell python3-config --includes)  \
	-I$(shell python3 -c 'import numpy; print(numpy.get_include())')/numpy	

#options and macro for the compilation. Do not put optimisation
#or debug option in there as it will interfere with the debug task and all task defined
# in the common parts of the makefiles
OPTIONS	= -std=c++0x -mtune=native -DOSX -fPIC -mtune=native -fopenmp

#flags and search path for the linker.
LDFLAGS 	= -shared -W -fPIC -fopenmp

#the resulting executable
EXEC	= $(HOME)/lib/qcm_ED.so

#the common core of the makefiles
include ../qcm_ED_object_list.txt
include ../makefile_nodep.mk

