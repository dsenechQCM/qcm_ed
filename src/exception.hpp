#ifndef exception_h
#define exception_h

#include <string>

void check_signals();
void qcm_ED_throw(const std::string& s);
void qcm_ED_catch(const std::string& s);
void qcm_throw(const std::string& s);
void qcm_catch(const std::string& s);

#endif
