/**
 Test file for the QCM library
 */

#include <Python.h>
#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

#ifdef _OPENMP
  #include <omp.h>
#endif

int main(int argc, char *argv[]){
	
  cout << omp_get_max_threads() << "open_MP threads" << endl;

  const char *prefix = "import sys\nsys.path.append(\"";
  const char *suffix = "\")\n";
  char *command = (char*)malloc(strlen(prefix)
                                + strlen(argv[1])
                                + strlen(suffix)
                                + 1);
  if (! command) {
    return -1;
  }
  strcpy(command, prefix);
  strcat(command, argv[1]);
  strcat(command, suffix);
  
  char *file = (char*)malloc(strlen(argv[1])+ strlen(argv[2]) + 1);
  strcpy(file, argv[1]);
  strcat(file, "/");
  strcat(file, argv[2]);

  FILE* f = fopen(file, "r");
  Py_Initialize();
  PyRun_SimpleString(command);
  PyRun_SimpleFile(f, file);
  Py_Finalize();
}
