List of functions
#################


.. py:function:: complex_HS(label=0)

	Returns true (nonzero int) is the Hilbert space is complex, false otherwise.

    :param int label: label of the model instance
    :return: True or False


.. py:function:: Green_function_dimension(label=0)

	Returns the dimension of the Green function matrix.

    :param int label: label of the model instance
    :return: None

.. py:function:: Green_function_solve(label=0)

	Applies the Green function solver to a particular instance of the model.
	This must be called (explicitly or implicitly) before computing the Green function at various frequencies

    :param int label: label of the model instance
    :return: None


.. py:function:: Green_function(z [, spindown=False, label=0])

    computes the Green function matrix at a given complex frequency.

    :param complex z: a complex frequency
    :param bool spindown: True if one computes the spin-down sector. Applies to the mixing case no 4 only.
    :param int label: label of the model instance
    :return: A dimension-2 NumPy array of complex numbers


.. py:function:: cluster_averages(label=0)

    Computes the ground state averages and variances of the operators defined in the model

    :param int label: label of the model instance
    :return: A dict of 2-tuples {name: (average, variance)}


.. py:function:: ground_state_solve(label=0)

    Computes the ground state of the model

    :param int label: label of the model instance
    :return: None


.. py:function:: hopping_matrix(spindown=False, label=0)

    Returns the one-body matrix of the model

    :param bool spindown: True if one computes the spin-down sector. Applies to the mixing case no 4 only.
    :param int label: label of the model instance
    :return: A dimension-2 NumPy array of complex numbers


.. py:function:: hybridization(label=0])

    Returns the Lehmann representation of the hybridization function
    :param int label: label of the model instance
    :return: A 2-tuple of
        1. the array of M real eigenvalues, M being the number of poles in the representation
        2. a rectangular (L x M) matrix (real of complex), L being the dimension of the Green function

.. py:function:: hybridization_function(z [, spindown=False, label=0])

    Computes the hybridization_function matrix at a given complex frequency. For models with baths only.

    :param complex z: a complex frequency
    :param bool spindown: True if one computes the spin-down sector. Applies to the mixing case no 4 only.
    :param int label: label of the model instance
    :return: A dimension-2 NumPy array of complex numbers


.. py:function:: mixing(label=0)

    Returns the mixing label of the instance

    :param int label: label of the model instance
    :return: 0,1,2,3 or 4, depending on mixing state:
        - 0 is the unmixed case
        - 1 is the anomalous case (without spin flip)
        - 2 is the spin flip case (without anomalous terms)
        - 3 is the anomalous and spin flip case
        - 4 is the normal case where spin up and spin down are different.


.. py:function:: model_size(name)

    Returns the number of cluster sites and bath sites.

    :param str name: name of the model
    :return: a 2-tuple of integers: the number of cluster sites, and the number of bath sites


.. py:function:: new_model_from_file(file)

    Constructs a model from a description written on file

    :param str file: name of the file containing the model description. The file name should have the extension .cluster, but the extension must be omitted from the argument
    :return: None


.. py:function:: new_model(name, nsites, nbath, [generators])

    Initiates a model, without defining operators

    :param str name: name of the model
    :param int nsites: number of cluster sites
    :param int nbath: number of bath sites
    :param generators: the symmetry generators of the cluster
    :type generators: 2-D list of integers (permutations of the range 1..nsites+nbath)
    :return: None


.. py:function:: new_model_instance(name, val, sec, label=0)

    Creates a new instance of the model

    :param str name: name of the model
    :param val: values of the parameter (dict of names:values)
    :param str sec: target Hilbert space sectors
    :param int label: label to be given to the instance
    :return: None


.. py:function:: new_operator(model, name, type, elements)

    Creates a new real operator within the model

    :param str model: name of the model
    :param str name: name of the operator
    :param str type: type of the operator ('one-body', 'anomalous', 'interaction', 'Hund', 'Heisenberg')
    :param str elements: array of real matrix elements (each element of the array is a 3-tuple)
    :return: None

    Note that the matrix elements must obey the following constraints: 
    - the first index is not larger than the second index. All operators are Hermitian or symmetric (or antisymmetric for anomalous operators). Thus just half the information is required and this is hard coded here.
    - The labels start at 1, no zero (even though they start at zero internally).


.. py:function:: new_operator_complex(model, name, type, elements)

    Creates a new complex operator within the model

    :param str model: name of the model
    :param str name: name of the operator
    :param str type: type of the model ('one-body', 'anomalous', 'interaction', 'Hund')
    :param str elements: array of complex matrix elements (each element of the array is a 3-tuple)
    :return: None

    The matrix elements are under the same constraints as for the previous function.

.. py:function:: one_body_solve(label=0)

    Solves the model, ignoring the interaction terms.

    :param int label: label of the model instance
    :return: None



.. py:function:: parameters(label=0)

    Returns a dictionary of the parameter values

    :param int label: label of the model instance
    :return: a dict of (str, float)




.. py:function:: print_models()

    Prints the model descriptions to the screen


.. py:function:: write_instance(file, label=0])

    Writes the solved model instance to a human-readable text file.
    Can be used to sidestep solving the model again

    :param str file: name of the file
    :param int label: label of the model instance
    :return: None

.. py:function:: qmatrix(label=0])

    Returns the Lehmann representation of the Green function
    :param int label: label of the model instance
    :return: A 2-tuple of
        1. the array of M real eigenvalues, M being the number of poles in the representation
        2. a rectangular (L x M) matrix (real of complex), L being the dimension of the Green function


.. py:function:: read_instance(file, label=0])

    Reads a solved model instance from a human-readable text file previously written by write_instance()
    Can be used to sidestep solving the model again
    
    :param str file: name of the file
    :param int label: label of the model instance
    :return: None



.. py:function:: self_energy(z [, spindown=False, label=0])

    computes the self-energy matrix at a given complex frequency.

    :param complex z: a complex frequency
    :param bool spindown: True if one computes the spin-down sector. Applies to the mixing case no 4 only.
    :param int label: label of the model instance
    :return: A dimension-2 NumPy array of complex numbers



.. py:function:: set_global_parameter(name, value)

    Sets the value of a global parameter

    :param str name: name of the parameter
    :param value: value to be given (leave blank to set a boolean parameter to True)
    :type value: (int or float or str)
    :return: None


.. py:function:: susceptibility_poles(name, label=0)

    Computes the poles of the dynamic susceptibility of an operator

    :param str name: name of the operator
    :param int label: label of the model instance
    :return: NumPy array of dimension (N,2), N being the number of poles. The other dimension contains the poles and the corresponding residues.


.. py:function:: susceptibility(name, freq, label=0)

    Computes the dynamical susceptibility of an operator

    :param str name: name of the operator
    :param freq: array of complex frequencies
    :type freq: NumPy array
    :param int label:  label of the model instance
    :return: a NumPy array of complex susceptibilities corresponding to the array of frequencies
