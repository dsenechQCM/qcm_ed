Introduction
############

What is qcm_ED ?
================

The qcm_ED module is a collection of functions that solves small electron systems with exact diagonalization. It uses full diagonalization, the Lanczos method, or the Davidson method. It also computes the Green function as a function of any complex-valued frequency, as well as the dynamic susceptibility of an operator.

qcm_ED is required by qcm, as its exact diagonalization solver. It interacts with qcm via a small number of interface functions that do not involve any custom data structures. It can also be interfaced with Python, but it is expected that its main use will be through qcm.

The compiling requirements for qcm are simple: (1) a C++ compiler and (2) BLAS-LAPACK, Needed for efficient vector and matrix operations.

Models solved by qcm_ED
=======================

The models solved by qcm_ED have :math:`N_s` cluster sites and :math:`N_b` bath sites, for a total of :math:`N_o = N_s+N_b` orbitals, each with two spin projections. The number of fermionic degrees of freedom is thus :math:`2N_o`. Orbitals in qcm_ED have no position. The model Hamiltonian is made of a combination of different terms of the following types:

One-body operators
------------------

.. math::
    \hat O = \sum_{\alpha,\beta} t_{\alpha\beta} c_\alpha^\dagger c_\beta


where :math:`\alpha`, :math:`\beta` are one-body indices that stand for orbital and spin. The matrix :math:`t_{\alpha\beta}` must be Hermitian: :math:`t_{\alpha\beta}=t^*_{\beta\alpha}`.

Anomalous operators
-------------------

.. math::
    \hat O = \sum_{\alpha,\beta} f_{\alpha\beta} c_\alpha c_\beta + f_{\alpha\beta}^* c_\beta^\dagger c_\alpha^\dagger

where the matrix :math:`f` is antisymmetric: :math:`f_{\alpha\beta} = -f_{\beta\alpha}`.

Interaction operators
---------------------

.. math::
    \hat O = \sum_{\alpha,\beta} V_{\alpha\beta} n_\alpha n_\beta

where :math:`n_\alpha = c_\alpha^\dagger c_\alpha` is the number operator for fermion :math:`\alpha`. The matrix :math:`V_{\alpha\beta}` must be real symmetric.

Hund operators
--------------

.. math::
    \hat O = \sum_{i,j} J_{ij} H_{ij}

where :math:`i` and :math:`j` stand for site indices.

.. math::
    H_{ij} = -n_{i\uparrow}n_{j\uparrow} - n_{i\downarrow}n_{j\downarrow} + c^\dagger_{i\uparrow}c_{j\uparrow}c^\dagger_{j\downarrow}c_{i\downarrow}
    + c^\dagger_{j\uparrow}c_{i\uparrow}c^\dagger_{i\downarrow}c_{j\downarrow}
    + c^\dagger_{i\uparrow}c_{j\uparrow}c^\dagger_{i\downarrow}c_{j\downarrow}
    + c^\dagger_{j\uparrow}c_{i\uparrow}c^\dagger_{j\downarrow}c_{i\downarrow}

See, e.g., A. Liebsch and  T. A. Costi, The European Physical Journal B, Vol. 51, p. 523 (2006).
This can also be written as

.. math::
    H_{ij} = -n_{i\uparrow}n_{j\uparrow} - n_{i\downarrow}n_{j\downarrow}
    + (c^\dagger_{i\uparrow}c_{j\uparrow}+\mathrm{H.c.})(c^\dagger_{i\downarrow}c_{j\downarrow}+ \mathrm{H.c.})

or as

.. math::
    H_{ij} = -c^\dagger_{i\uparrow}c^\dagger_{j\uparrow}c_{j\uparrow}c_{i\uparrow} - c^\dagger_{i\downarrow}c^\dagger_{j\downarrow}c_{j\downarrow}c_{i\downarrow}
    + c^\dagger_{i\uparrow}c^\dagger_{i\downarrow}c_{j\downarrow}c_{j\uparrow}
    + c^\dagger_{j\uparrow}c^\dagger_{j\downarrow}c_{i\downarrow}c_{i\uparrow}
    - c^\dagger_{j\uparrow}c^\dagger_{i\downarrow}c_{i\uparrow}c_{j\downarrow}
    - c^\dagger_{i\uparrow}c^\dagger_{j\downarrow}c_{j\uparrow}c_{i\downarrow}



Bath orbitals
=============

The system is separated in two sub-systems:

1) The cluster per se, with :math:`N_c` orbitals, supports interactions and its Green function is computed.

2) The bath, with :math:`N_b` orbitals, does not support interactions and its Green function is not computed; it is there only to serve the CDMFT/CDIA methods of \libqcm. The hybridization function :math:`\Gamma(\omega)` between the bath and cluster can be computed.

One-body or anomalous operators are defined either on the cluster per se, or on the bath, or as a hybridization between the two.

Cluster symmetries
==================

The Hamiltonian may benefit from discrete, point group symmetries. qcm_ED takes advantage of Abelian symmetries only. They are specified by a finite number of commuting generators, in the form of permutations of sites with optional phases.

Mixing, site and orbital labels
===============================

By convention, orbitals (or degrees of freedom) within a cluster are numbered and labelled consecutively as follows:
- From 0 to :math:`N_s-1`, the spin up orbitals of the cluster proper.

- From :math:`N_s` to :math:`N_o-1`, the spin up orbitals of the bath.

- From :math:`N_o` to :math:`N_o+N_s-1`, the spin down orbitals of the cluster proper.

- From :math:`N_o+N_s` to :math:`2N_o-1`, the spin down orbitals of the bath.

The above scheme describes the indices labeling the degrees of freedom.
A slightly different scheme labels the indices of the Green function, depending on the mixing options:


- If there are no anomalous terms nor spin-flip terms in the model, the Green function (cluster or lattice) then does not mix up and down spins and the Gorkov function vanishes.
The Green function is then block diagonal, with two :math:`N_s\times N_s` blocks :math:`\mathbf{G}_\uparrow` and :math:`\mathbf{G}_\downarrow`.
The destructions operators form two arrays :math:`(c_{i\uparrow})` and :math:`(c_{i\downarrow})` (:math:`i=0,\dots,N_s-1`).
If the Hamiltonian is symmetric under the exchange of up and down spins, then only :math:`\mathbf{G}_\uparrow` is computed and :math:`\mathbf{G}_\downarrow=\mathbf{G}_\uparrow`.
This is known as mixing state 0.
If the Hamiltonian is not symmetric under the exchange of up and down spins, then :math:`\mathbf{G}_\uparrow` and :math:`\mathbf{G}_\downarrow` are computed in turn.
This is known as mixing state 4.

- If there are spin-flip terms, but sill no anomalous terms, the cluster Green function is a :math:`2N_s\times 2N_s` matrix, associated with the destructions operators forming an array :math:`(c_{i\uparrow})\oplus(c_{i\downarrow})` (:math:`i=0,\dots,N_s-1`).
This is known as mixing state 2.

- If there are anomalous terms, but no spin-flip terms, the cluster Green function is a :math:`2N_s\times 2N_s` matrix, associated with the destruction and creation operators forming an array :math:`(c_{i\uparrow})\oplus(c^\dagger_{i\downarrow})` (:math:`i=0,\dots,N_s-1`).
This is known as mixing state 1.

- If there are both anomalous and spin-flip terms, the cluster Green function is a :math:`4N_s\times 4N_s` matrix, associated with the destruction and creation operators forming an array :math:`(c_{i\uparrow})\oplus(c_{i\downarrow})\oplus(c^\dagger_{i\uparrow})\oplus(c^\dagger_{i\downarrow})` (:math:`i=0,\dots,N_s-1`).
This is known as mixing state 3.


Hilbert space sectors
---------------------

An instance of the model is set by the values of the parameters, and the list of Hilbert space sectors to probe for the ground state.
The format for each sector is R<irrep>:N<number>:S<number>.
For instance:

- `R0:N5:S-1` means irreducible representation 0, :math:`N=5` electrons and total spin :math:`S=-\frac12`.

- `R1:N12:S2` means irreducible representation 1, :math:`N=12` electrons and total spin :math:`S=1`.

- `R1:S2` means irreducible representation 1, total spin :math:`S=1` and a nonconserved number of electrons. Likewise,

- `R0:N12` means irreducible representation 0, 12 electrons and a nonconserved value of :math:`S_z`.

Many sectors can be specified for each cluster, separated by `/`.
Irreps are labeled from 0 (the trivial irrep) to :math:`g-1`, :math:`g` being the number of elements in the point group considered.
Acceptable values for the number of electrons range from 0 to :math:`2N_s`.
Acceptable values for :math:`2S_z` range from :math:`-2N_s` to :math:`2N_s`.
