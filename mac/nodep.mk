#Your favorite compiler goes here.
COMPILER	= c++

#linker; Should be the compiler in most cases.
#A notable exception in making a MPI app using clang to compile, then the linker should be mpicxx
LINKER	= $(COMPILER)

PYPREFIX = $(shell python3-config --prefix)

#dynamic link library will vary quite a bit from one platform to the other
LINK	= -framework Accelerate \
	$(shell python3-config --ldflags) -lomp -lpython3.8

#include path, should be left empty for most platform
LOCAL_INCLUDE 	= $(shell python3-config --includes)  \
	-I$(shell python3 -c 'import numpy; print(numpy.get_include())')/numpy \

#options and macro for the compilation. Do not put optimisation
#or debug option in there as it will interfere with the debug task and all task defined
# in the common parts of the makefiles
OPTIONS	= -std=c++17 -mtune=native -DOSX -fPIC  -Xpreprocessor -fopenmp

#flags and search path for the linker.
LDFLAGS 	= -shared -Wl -fPIC 

#the resulting executable
EXEC	= $(HOME)/lib/qcm_ED.so

#the common core of the makefiles
include ../qcm_ED_object_list.txt
include ../makefile_nodep.mk

