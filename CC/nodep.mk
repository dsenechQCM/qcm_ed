#Your favorite compiler goes here.
COMPILER	= g++

LINKER	= $(COMPILER)

#dynamic link library will vary quite a bit from one platform to the other
LINK = -lstdc++ $(python3-config --ldflags) \
	-lmkl -liomp5

#include path, should be left empty for most platform
LOCAL_INCLUDE = $(shell python3-config --includes)  -I$(shell python -c 'import numpy; print(numpy.get_include())')/numpy

OPTIONS	= -fopenmp -std=c++11 -fPIC -fpermissive

LDFLAGS 	= -fPIC -shared -fopenmp

EXEC	= $(HOME)/lib/qcm_ED.so

#the common core of the makefiles
include ../qcm_ED_object_list.txt
include ../makefile_nodep.mk
